@extends('admin.admin_master')
@section('admin')

<div class="container-fluid">

    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User Show</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form action="{{ route('user.edit', $users->id) }}" method="post">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="{{ $users->name }}">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Enter email" value="{{ $users->email }}">
              </div>

              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" value="{{ $users->password }}">
              </div>
              <div class="form-group">
                <label for="phone_number">Phone Number</label>
                <input type="number" class="form-control" name="phone_number" placeholder="phone_number" value="{{ $users->phone_number }}">
              </div>
              <div class="form-group">
                <label for="password">Address</label>
                <input type="text" class="form-control" name="address" placeholder="Address" value="{{ $users->address }}">
              </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('user.index') }}" class="btn btn-warning">Cancel</a>
            </div>
          </form>
        </div>

      </div>


</div>

@endsection

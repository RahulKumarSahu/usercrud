@extends('admin.admin_master')
@section('admin')

<div class="container-fluid">

    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User Show</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form action="{{ route('user.update', $users->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="card-body">

              <div class="form-group @error('email') has-error @enderror">
                <label for="name">Name</label>
                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="{{ $users->name }}">
                @error('email')
                    <span class="text-danger"> {{ $message }} </span>
                @enderror
              </div>

              <div class="form-group @error('email') has-error @enderror">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Enter email" value="{{ $users->email }}">
                @error('email')
                    <span class="text-danger"> {{ $message }} </span>
                @enderror
              </div>

              <div class="form-group @error('password') has-error @enderror">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" value="{{ $users->password }}">
                @error('password')
                <span class="text-danger"> {{ $message }} </span>
            @enderror
              </div>

              <div class="form-group @error('phone_number') has-error @enderror">
                <label for="phone_number">phone_number</label>
                <input type="number" class="form-control" name="phone_number" placeholder="phone_number" value="{{ $users->phone_number }}">
                @error('phone_number')
                <span class="text-danger"> {{ $message }} </span>
            @enderror
              </div>

              <div class="form-group @error('address') has-error @enderror">
                <label for="address">Address</label>
                <input type="text" class="form-control" name="address" placeholder="address" value="{{ $users->address }}">
                @error('address')
                <span class="text-danger"> {{ $message }} </span>
            @enderror
              </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>

            </div>
          </form>
        </div>

      </div>


</div>

@endsection
